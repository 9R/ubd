{ stdenv, glib, pkg-config,minixml, json_c }:
stdenv.mkDerivation rec {
  name = "uberbus-${version}";
  version = "0.2.0";

  src = ./.;

  nativeBuildInputs = [ glib pkg-config minixml json_c ];
  buildInputs = [ ];

  buildPhase = ''
    ./configure
    make
  '';

  installPhase = ''
    mkdir -p $out/bin
    cp src/ubd $out/bin
    cp src/ubnetd $out/bin
  '';
}

